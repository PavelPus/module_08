@Library("shared-library") _

def jenkins_path = "/var/lib/docker/mapped"
def ansible_path = "/etc"
key_path = "/etc/ansible/ansible-playbooks/filenet/workspace/FileNet/.ssh"
def passwork_vault = 'ОЭСЭД'
def azure_username = 'DC-MGMT\\KozhevnikovAV'
def repo_path = '/tfs/FileNet/ascui/_git/ascui'
def extra_vars = [:]

def subject_ost_map  = [
        pao: 'ПАО',
        tnt: 'ТНТ',
        tne: 'ТНЭ',
        tdr: 'ТДР',
        tnpk: 'ТНПК',
        tpv: 'ТПВ',
        tpp: 'ТПП',
        tsd: 'ТСД',
        tnl: 'ТНЛ',
		tur: 'ТУР',
		tdv: 'ТДВ',
		tnb: 'ТНБ',
		tvv: 'ТВВ',
		tsib: 'ТСИБ',
		chtn: 'ЧТН',
		tzs: 'ТЗС',
		tpk: 'ТПК',
]
def subject_kis_map = [
        ehd: 'КИС ЭХД',
        muzedo: 'МЮЗ ЭДО',
        shod: 'СХОД',
		eatd: 'ЭАТД',
]

pipeline {
    agent  { label 'BuiltInNode' }
    parameters {
        string(name: 'kis_tag')
        string(name: 'ost_tag')
        string(name: 'ascui_tuz_name')
        string(name: 'ascui_tuz_tag')
        string(name: 'recipient', defaultValue: 'CR-KISEHD_Admin@transneft.ru')
        string(name: 'error_recipient', defaultValue: 'kozhevnikovav@transneft.ru')
		string(name: 'win_user', defaultValue: 'no')
		string(name: 'limit', defaultValue: '')
		string(name: 'extras', defaultValue: '')
		string(name: 'branch', defaultValue: 'master')
    }
    stages {
        stage('Получение пользователей и паролей из Passwork') {
            agent { label 'BuiltInNode' }
            steps {
                script {
                    withCredentials([string(credentialsId: 'passwork_api_key', variable: 'passwork_api_key')]) {
                        pw = new passworkGroovy(passwork_api_key);
                        vault_id = pw.search_vault(passwork_vault)
                        result_size = vault_id.size()
                        if (result_size == 1) {
                            search_result = pw.search_password('IBM WAS Admin', false, vault_id[0].id, ["${params.kis_tag}_${params.ost_tag}"])
                            was_admin_name = search_result[0].login
                            was_admin_password = search_result[0].password
							extra_vars.put('was_admin_name', was_admin_name)
							extra_vars.put('was_admin_password', was_admin_password)
                            search_result = pw.search_password('Mail User', false, vault_id[0].id, ["ascui","mailuser"])
                            mail_user_name = search_result[0].login
                            mail_user_password = search_result[0].password
							extra_vars.put('mail_user', mail_user_name)
							extra_vars.put('mail_password', mail_user_password)
                            search_result = pw.search_password("${params.become_tuz_name}", false, vault_id[0].id, ["ascui","${params.become_tuz_tag}"])
                            become_user = search_result[0].login
                            become_user_password = search_result[0].password
							extra_vars.put('windows_become_user', become_user)
							extra_vars.put('become_password', become_user_password)
							if (params.win_user != 'no') {
								search_result = pw.search_password("ТУЗ для ${params.win_user.toUpperCase()}", false, vault_id[0].id, ["ascui_${params.win_user}"])
								windows_user = search_result[0].login
								windows_password = search_result[0].password
							}
							else {
								windows_user = become_user
								windows_password = become_user_password
							}
							extra_vars.put('windows_user', windows_user)
							extra_vars.put('windows_password', windows_password)
                            if (params.kis_tag == 'ehd' || params.kis_tag == 'eatd') {
                                search_result = pw.search_password('Review User', false, vault_id[0].id, ["${params.kis_tag}_${params.ost_tag}"])
                                review_user_name = search_result[0].login
                                review_user_password = search_result[0].password
								extra_vars.put('review_user', review_user_name)
								extra_vars.put('review_password', review_user_password)
                                search_result = pw.search_password('Abbyy DB User', false, vault_id[0].id, ["${params.kis_tag}_${params.ost_tag}"])
                                abbyydb_user_name = search_result[0].login
                                abbyydb_user_password = search_result[0].password
								extra_vars.put('abbyy_db_user', abbyydb_user_name)
								extra_vars.put('abbyy_db_password', abbyydb_user_password)
                            }
                            else {
                                review_user_name = ""
                                review_user_password = ""
                                abbyydb_user_name = ""
                                abbyydb_user_password = ""
                            }
                        }
                        else {
                            throw new Exception("""\
                            Поиск хранилищ вернул результат больше 1-го (${result_size})
                            Результат: ${vault_id}\
                            """)
                        }
                    }
                }
               
            }
        }
        stage('Запуск плэйбука ppr.yml') {
            agent { label 'ansible_filenet' }
            steps {
				script {
                    pwd=sh(returnStdout: true, encoding: 'UTF-8', script: '''pwd''').trim()
					repo_suffix = "${params.kis_tag}_${params.ost_tag}/ascui"
                    anspath = "${pwd}/${repo_suffix}/"
                    echo pwd
					echo anspath
					sh "rm -rf ${anspath}"
                    workspace_home = "/etc/ansible/ansible-playbooks/filenet/workspace/FileNet"
                    username = 'DC-MGMT\\KozhevnikovAV'
                    repo_path = '/tfs/FileNet/ascui/_git/ascui'
                    sh 'GIT_SSH_COMMAND="ssh -oKexAlgorithms=diffie-hellman-group1-sha1,diffie-hellman-group14-sha1 -oUser=' + username + ' -i ' + workspace_home + '/.ssh/id_rsa" git clone ssh://vdc01-petttfs01.dc-prod.tn.corp:22' + repo_path + " ${repo_suffix}"
                    sh "cd ${repo_suffix} && git checkout ${params.branch}"
                }
				catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
					ansiblePlaybook(
						colorized: true,
						playbook: anspath + '/ppr.yml',
						inventory: anspath + "/inventories/${params.ost_tag}/${params.kis_tag}/hosts",
						credentialsId: params.ascui_tuz_tag,
						limit: params.limit,
						extraVars: extra_vars,
						extras: params.extras
					)
					script {
						env.html_content = readFile"/tmp/mail_body_${params.kis_tag}_${params.ost_tag}.html"
					}
				}
            }
            post {
				success {
					echo "Выполнено успешно"
					echo "${repo_suffix}/roles/ppr/send_html_email/files/P8CE/img/**"
					stash name: 'images', includes: "${repo_suffix}/roles/ppr/send_html_email/files/P8CE/img/**"
				}
            }
        }
        stage('Получение изображений и отправка почты') {
           agent { label 'BuiltInNode' }
           steps {
				dir("img") {
					unstash 'images'
				}
                script {
                        emailext body: '''${SCRIPT, template="email_empty.html"}''', 
                                 attachmentsPattern: "img/${repo_suffix}/roles/ppr/send_html_email/files/P8CE/img/*.png",
                                 subject: "ППР ${subject_kis_map[params.kis_tag]} ${subject_ost_map[params.ost_tag]}",
                                 mimeType: 'text/html', 
                                 to: params.recipient
                                 saveOutput: false
                }
                dir('img') {
                    deleteDir()
                }
           }
        }
    }
    post {
        failure{
            emailext body: "ППР ${subject_kis_map[params.kis_tag]} ${subject_ost_map[params.ost_tag]}<br>${currentBuild.currentResult}: Job ${env.JOB_NAME}<br><b>Pipeline:</b> ${currentBuild.fullDisplayName}<br><b>Build:</b> ${currentBuild.duration}",
                     subject: "Jenkins build: ${env.JOB_NAME} (${currentBuild.currentResult})",
                     mimeType: 'text/html', 
                     to: params.error_recipient,
                     saveOutput: false
        }
    }
}