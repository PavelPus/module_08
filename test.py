from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import time

url='http://vdc01-petehd632.dc-prod.tn.corp/GP/'
options = Options()
options.add_argument("window-size=1500,800")
cService = webdriver.ChromeService(executable_path='chromedriver.exe')
driver = webdriver.Chrome(options=options, service=cService)
driver.get(url)
time.sleep(50)
client_response = driver.page_source
soup = BeautifulSoup(client_response, 'lxml')
print(soup.prettify())
driver.quit()

Недоступен сервер DSS компании (внутренний компонент МЮЗ ЭДО)