import ldap3
import sys

user = sys.argv[1]
userp = sys.argv[2]

domain_server = ldap3.Server('VDC01-PEDC---01.tn.tngrp.ru', 636, use_ssl=True)
conn = ldap3.Connection(domain_server, user, userp, raise_exceptions=True) 
conn.start_tls()
try:
    conn.bind()
    print('Учетная запись проверена успешно.')
except ldap3.core.exceptions.LDAPInvalidCredentialsResult:
    print('Пароль или пользователь AD не верны.')

# 'PushkarevPA-A@tn.tngrp.ru'
# 'ek4LJichSQ~+k3dEhlOi'