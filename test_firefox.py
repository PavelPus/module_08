from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import time

#url = 'https://muzedo-tnl.tn.tngrp.ru/UZDO/index.html' # TNL UZDO
url = 'https://myzedo-tnt.tn.tngrp.ru/UZDO/index.html' # TNT UZDO

####Cred
tnlogin = 'PushkarevPA-A@tn.tngrp.ru'
tnpas = 'e8E8Xfup1lvmEyV*xv('
dclog = 'PushkarevPA@dc-mgmt.tn.corp'
dcpas = 'pvJ^9kePqFpX[]^3!QPo'

def get_log_pas(url):
    logpas = []
    #if 'tngrp' in url or 'dc-prod' in url:
    if 'dc-prod' in url:
        logpas.append(dclog)
        logpas.append(dcpas)
    else:
        logpas.append(tnlogin)
        logpas.append(tnpas)    

    return logpas

cred = get_log_pas(url)
print('Логин={0} и пароль={1}'.format(cred[0].partition('@')[0], cred[1]))

cService = webdriver.FirefoxService('geckodriver.exe')
driver = webdriver.Firefox(service=cService)
driver.implicitly_wait(300)
wait =WebDriverWait(driver, 600)
t0 = time.time()
print('T0={0}'.format(t0))
driver.get(url)
driver.maximize_window()
wait.until(EC.element_to_be_clickable((By.XPATH, "//span[contains(text(), 'Войти')]")))
t1 = time.time()
print('T1={0}'.format(t1))
time_spended = t1 - t0
print(f"Login page time: {time_spended:.2f} seconds")
#time.sleep(5)
#client_response = driver.page_source
#soup = BeautifulSoup(client_response, 'lxml')

#el_login = soup.find(name='input', attrs={'name': 'loginName'})
#el_pass = soup.find(name='input', attrs={'type': 'password'})

el_login = driver.find_element(By.CSS_SELECTOR, 'input[name="loginName"]')
el_pass = driver.find_element(By.CSS_SELECTOR, 'input[type="password"]')
el_domain_selector = driver.find_element(By.CSS_SELECTOR, 'input[value="▼ "]')
el_login_button = driver.find_element(By.XPATH, "//span[contains(text(), 'Войти')]")
#el_inbutton = driver.find_element(By.XPATH, "//div[contains(text(), 'Войти')]")
#print(el_login)

el_login.send_keys(cred[0].partition('@')[0])
el_pass.send_keys(cred[1])
el_domain_selector.click()

#time.sleep(1)
#el_domain = driver.find_element(By.XPATH, "//td[contains(text(), '@tn.tngrp.ru')]") # ЭХД
el_domain = driver.find_element(By.XPATH, "//div[contains(text(), '@tn.tngrp.ru')]") # мюзда
el_domain.click()
time.sleep(1)
#print(el_inbutton)
t0 = time.time()
print('T0={0}'.format(t0))
el_login_button.click()
wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(text(), 'Операции над ЮЗЭД')]")))
#wait.until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/div[3]/div/div/div[1]/div/div[3]/div/div[2]/div[7]/div[1]")))
t1 = time.time()
print('T1={0}'.format(t1))
time_spended = t1 - t0
print(f"Main page time: {time_spended:.2f} seconds")
time.sleep(5)

el_left = driver.find_element(By.XPATH, '//*[@id="dijit_layout_ContentPane_0"]')
print(el_left)
driver.execute_script("el_left.scrollIntoView(true)")

el_expand_button = driver.find_element(By.XPATH, '/html/body/div[1]/div[3]/div/div/div[1]/div/div[3]/div/div[2]/div[7]/div[1]')
#time.sleep(5)
print(el_expand_button.is_displayed() )
print(el_expand_button.is_enabled())
#driver.execute_script("arguments[0].style.visibility='hidden'", element)
#driver.execute_script("el_expand_button.scrollIntoView(true)")
el_expand_button.click()
time.sleep(5)
el_journal = driver.find_element(By.XPATH, '/html/body/div[1]/div[3]/div/div/div[1]/div/div[3]/div/div[2]/div[7]/div[1]/span[1]')
print(el_journal)
el_journal.click()
time.sleep(5)
el_journal_integration_button = driver.find_element(By.XPATH, '/html/body/div[1]/div[3]/div/div/div[1]/div/div[3]/div/div[2]/div[7]/div[2]/div[2]/div[1]')
print(el_journal_integration_button)
el_journal_integration_button.click()
time.sleep(5)
el_journal_integration = driver.find_element(By.XPATH, '/html/body/div[1]/div[3]/div/div/div[1]/div/div[3]/div/div[2]/div[7]/div[2]/div[2]/div[1]/span[1]')
print(el_journal_integration.is_displayed())
el_journal_integration.click()
#print(soup.prettify())
#driver.quit()