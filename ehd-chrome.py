from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import time
import random
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

url = 'https://kisehd-tnt.tn.tngrp.ru/GP/index.html' # TNT EHD
# url = 'https://npk01-piehd--01.npk.tn.corp/GP/index.html' # TNPK EHD

####Cred
tnlogin = 'PushkarevPA-A@tn.tngrp.ru'
tnpas = 'k&zO[zQAsA5YKY06g!x)'
dclog = 'PushkarevPA@dc-mgmt.tn.corp'
dcpas = 'pvJ^9kePqFpX[]^3!QPo'

def get_log_pas(url):
    logpas = []
    #if 'tngrp' in url or 'dc-prod' in url:
    if 'dc-prod' in url:
        logpas.append(dclog)
        logpas.append(dcpas)
    else:
        logpas.append(tnlogin)
        logpas.append(tnpas)    

    return logpas

cred = get_log_pas(url)
print('Логин={0} и пароль={1}'.format(cred[0].partition('@')[0], cred[1]))

cService = webdriver.ChromeService('.\.venv\Lib\site-packages\chromedriver_py\chromedriver_win64.exe')
driver = webdriver.Chrome(service=cService)
driver.implicitly_wait(300)
wait =WebDriverWait(driver, 600)

# Открыть страницу логина
t0 = time.time()
print('T0={0}'.format(t0))
driver.get(url)
driver.maximize_window()
wait.until(EC.element_to_be_clickable((By.XPATH, "//span[contains(text(), 'Войти')]")))
t1 = time.time()
time_spended = t1 - t0
print(f"Login page time: {time_spended:.2f} seconds")

# Логин в КИС
el_login = driver.find_element(By.CSS_SELECTOR, 'input[name="loginName"]')
el_pass = driver.find_element(By.CSS_SELECTOR, 'input[type="password"]')
el_domain_selector = driver.find_element(By.CSS_SELECTOR, 'input[value="▼ "]')
el_login_button = driver.find_element(By.XPATH, "//span[contains(text(), 'Войти')]")
el_login.send_keys(cred[0].partition('@')[0])
el_pass.send_keys(cred[1])
el_domain_selector.click()
el_domain = driver.find_element(By.XPATH, "//td[contains(text(), '@tn.tngrp.ru')]") # ЭХД
el_domain.click()
time.sleep(1)
t0 = time.time()
el_login_button.click()
wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(text(), 'Прочие действия')]")))
t1 = time.time()
time_spended = t1 - t0
print(f"Main page time: {time_spended:.2f} seconds")
time.sleep(5)

# Поиск доков
driver.find_element(By.ID, 'dijit__TreeNode_1_label').click()
el_input_search = wait.until(EC.visibility_of_element_located((By.ID, 'dijit_form_ValidationTextBox_0')))
el_input_search.send_keys('5')
time.sleep(1)
wait.until(EC.visibility_of_element_located((By.ID, "dijit_form_Button_39"))).click()

# Просмотр карточки
time.sleep(5)
el_row_checkbox = wait.until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[1]/div[3]/div[2]/div/div[3]/div[1]/div/div[2]/div/div[3]/div[4]/div[1]/table/tbody/tr/td/span')))
el_row_checkbox.click()
el_span_button = driver.find_elements(By.CLASS_NAME, 'dijitButton')
for s in el_span_button:
    inner_span_text = s.find_element(By.CLASS_NAME, 'dijitButtonText')
    if inner_span_text.get_attribute('innerHTML') == 'Показать карточку':
        s.click()

# action = ActionChains(driver)
# inner_span_cursor = ''
# doc_card_button = ''
# while inner_span_cursor != 'pointer':
#     el_row_checkbox = wait.until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[1]/div[3]/div[2]/div/div[3]/div[1]/div/div[2]/div/div[3]/div[4]/div[1]/table/tbody/tr/td/span')))
#     el_row_checkbox.click()
#     time.sleep(1)
#     action.send_keys(Keys.SPACE).perform()
#     time.sleep(1)
#     el_row_checkbox.click()
#     doc_card_button = ''
#     el_span_button = driver.find_elements(By.CLASS_NAME, 'dijitButton')
#     #print(el_span_button)
#     for s in el_span_button:
#         inner_span_text = s.find_element(By.CLASS_NAME, 'dijitButtonText')
#         #print(inner_span_text.get_attribute('id') + '\t' + inner_span_text.get_attribute('innerHTML'))
#         if inner_span_text.get_attribute('innerHTML') == 'Показать карточку':
#             inner_span_cursor = s.find_element(By.CLASS_NAME, 'dijitButtonNode').value_of_css_property('cursor')
#             doc_card_button = s
#     time.sleep(random.randint(1,5))
# doc_card_button.click()
# Синхронизация с УС
#wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Ручная синхронизация УС – ЭХД')]"))).click()