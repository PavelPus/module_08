from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.firefox.options import Options
import time
import random
import sys
import datetime
import jinja2
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import os
import shutil
from sys import argv

# Для отчета
content = {}
check_doc_card = 'Неуспешно'

# Для проверки БП
test_files_path = 'C:\\selscript\\test_docs\\'
export_path = '\\\\VDC01-PEHDTNT14\\ICC_service_folder\\Export\\'
cur_name = 'CurOUP_Aup_TST'
cur_pass = 'Nka!0t!t19ZNqG#0'

# Урлы КИСов
#url = 'https://kisehd-tnt.tn.tngrp.ru/GP/index.html' # TNT EHD
url = 'https://npk01-piehd--01.npk.tn.corp/GP/index.html' # TNPK EHD
#url = 'https://kisehd-apl.tsd.tn.corp/GP/index.html' # TSD
#url = 'https://nsz01-piehd--01.kaz.tn.corp/GP/index.html' # TPK
#url = 'https://kisehd-tpv.tn.tngrp.ru/GP/index.html' #TPV
#url = 'https://ndc01-peehd--01.dc-prod.tn.corp/GP/index.html' #PAO
#url = 'https://nvv01-pikisehd1.nnov.tn.corp/GP/index.html' # TVV
#url = 'https://nsm01-piehd--01.uht.tn.corp/GP/index.html' # TSV
#url = 'https://ndc01-pehdtne01.dc-prod.tn.corp/GP/index.html' # TNE
#url='https://kisehd-tpp.tn.tngrp.ru/GP/index.html' # TPP
#url = 'https://nbn01-piehd--01.spb.tn.corp/GP/index.html' #TNB

content['datetimenow'] = datetime.datetime.now().strftime('%d.%m.%Y %H:%M')

####Cred

def gen_report(content):
    # Jinja2
    loader = jinja2.FileSystemLoader('.')
    env = jinja2.Environment(loader=loader, trim_blocks=True)

    tpl = env.get_template('template-ehd.txt')
    result = tpl.render(content)
    with open('otchet.html', 'wb') as fp:
        fp.write(result.encode('utf-8'))
    # Открыть страницу отчета
    driver.get('file:///Z:\УОПС\ОЭСЭД\FileNet\AL_Пушкарев\Scripts\otchet.html')

def get_log_pas(url):
    logpas = {}
    logpas['tnlog'] = ''
    logpas['tnpas'] = ''
    logpas['curlog'] = ''
    logpas['curpas'] = ''
    with open('cred') as f:
        for line in f:
            if 'tnlog' in line:
                logpas['tnlog'] = line[line.find('tnlog:') + len('tnlog:'):].strip().rstrip()
            if 'tnpas' in line:
                logpas['tnpas'] = line[line.find('tnpas:') + len('tnpas:'):].strip().rstrip()
            if 'curpas' in line:
                logpas['curpas'] = line[line.find('curpas:') + len('curpas:'):].strip().rstrip()
            if 'curlog' in line:
                logpas['curlog'] = line[line.find('curlog:') + len('curlog:'):].strip().rstrip()
    
    if logpas['tnlog'] == '':
        logpas['tnlog'] = 'PushkarevPA-A@tn.tngrp.ru'
    if logpas['tnpas'] == '':
        logpas['tnpas'] = 'FH!j-x3uw]PHaCYerY0m'
    if logpas['curlog'] == '':
        logpas['curlog'] = 'PushkarevPA-A@tn.tngrp.ru'
    if logpas['curpas'] == '':
        logpas['curpas'] = 'ek4LJichSQ~+k3dEhlOi'
    
    return logpas

def card_view(rows, doc_index: int):
    action = ActionChains(driver)
    inner_span_cursor = ''
    doc_card_button = ''
    while inner_span_cursor != 'pointer':
        if doc_index > 1:
            rows[doc_index - 1].click()
            time.sleep(1)
            action.send_keys(Keys.SPACE).perform()
        rows[doc_index].click()
        time.sleep(1)
        action.send_keys(Keys.SPACE).perform()
        time.sleep(1)
        rows[doc_index].click()
        doc_card_button = ''
        el_span_button = driver.find_elements(By.CLASS_NAME, 'dijitButton')
        #print(el_span_button)
        for s in el_span_button:
            inner_span_text = s.find_element(By.CLASS_NAME, 'dijitButtonText')
            #print(inner_span_text.get_attribute('id') + '\t' + inner_span_text.get_attribute('innerHTML'))
            if inner_span_text.get_attribute('innerHTML') == 'Показать карточку':
                inner_span_cursor = s.find_element(By.CLASS_NAME, 'dijitButtonNode').value_of_css_property('cursor')
                doc_card_button = s
        time.sleep(random.randint(1,5))
    doc_card_button.click()
    time.sleep(2)
    check_doc_card = 'Успешно'
    return check_us()

def check_us():
    global check_doc_card
    # Синхронизация с УС
    driver.switch_to.window(driver.window_handles[1])
    time.sleep(2)
    wait.until(EC.invisibility_of_element_located((By.ID, 'glass')))
    wait.until(EC.visibility_of_element_located((By.XPATH, "//button[contains(., 'Ручная синхронизация УС – ЭХД')]"))).click()
    time.sleep(2)
    driver.switch_to.window(driver.window_handles[2])
    time.sleep(2)
    wait.until(EC.visibility_of_element_located((By.XPATH, "//button[contains(., 'Синхронизировать')]"))).click()
    el_div_result = wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='gwt-HTML MainStyle-contentMargin']")))
    if 'Синхронизация документа завершена успешно' in el_div_result.get_attribute('innerHTML') or 'Статус пакета успешно отправлен в УС' in el_div_result.get_attribute('innerHTML'):
        res = True
    else:
        res = False
    check_doc_card = 'Успешно'
    driver.close()
    driver.switch_to.window(driver.window_handles[1])
    driver.close()
    driver.switch_to.window(driver.window_handles[0])
    return res
    #print(el_div_result.get_attribute('innerHTML'))

def doc_delete():
    action = ActionChains(driver)
    inner_span_cursor = ''
    doc_delete_button = ''
    try:
        el_rows_checkbox = wait.until(EC.visibility_of_any_elements_located((By.CSS_SELECTOR, "span[role=checkbox]")))
        while inner_span_cursor != 'pointer':
            time.sleep(1)
            el_rows_checkbox[1].click()
            time.sleep(1)
            action.send_keys(Keys.SPACE).perform()
            time.sleep(1)
            el_rows_checkbox[1].click()
            doc_delete_button = ''
            el_span_button = driver.find_elements(By.CLASS_NAME, 'dijitButton')
            for s in el_span_button:
                inner_span_text = s.find_element(By.CLASS_NAME, 'dijitButtonText')
                #print(inner_span_text.get_attribute('id') + '\t' + inner_span_text.get_attribute('innerHTML'))
                if inner_span_text.get_attribute('innerHTML') == 'Логическое удаление':
                    inner_span_cursor = s.find_element(By.CLASS_NAME, 'dijitButtonNode').value_of_css_property('cursor')
                    doc_delete_button = s
        doc_delete_button.click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[1])
        time.sleep(2)
        wait.until(EC.visibility_of_element_located((By.XPATH, "//button[contains(., 'Удалить')]"))).click()
        time.sleep(2)
        wait.until(EC.visibility_of_element_located((By.XPATH, "//button[contains(., 'Ok')]"))).click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[0])
        time.sleep(1)
        print('Удаление успешно. БП успешно.')
        content['base_bp'] = "Успешно"
    except:
        print('Проблемы с БП. Проверьте наличие тестового документа.')
        content['base_bp'] = "Неуспешно"

def login_action(name_login, name_pass):
    el_login = driver.find_element(By.CSS_SELECTOR, 'input[name="loginName"]')
    el_pass = driver.find_element(By.CSS_SELECTOR, 'input[type="password"]')
    el_domain_selector = driver.find_element(By.CSS_SELECTOR, 'input[value="▼ "]')
    el_login_button = driver.find_element(By.XPATH, "//span[contains(text(), 'Войти')]")
    el_login.send_keys(name_login)
    el_pass.send_keys(name_pass)
    el_domain_selector.click()
    el_domain = driver.find_element(By.XPATH, "//td[contains(text(), '@tn.tngrp.ru')]") # ЭХД
    el_domain.click()
    time.sleep(1)
    t0 = time.time()
    el_login_button.click()
    try:
        wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(text(), 'Прочие действия')]")))
        t1 = time.time()
        time_spended = t1 - t0
        content['main_page_time'] = round(time_spended, 2)
        print(f"Main page time: {time_spended:.2f} seconds")
    except:
        print(f"Главная страница КИС ЭХД недоступна.")
        content['main_page_time'] = "Главная страница КИС ЭХД недоступна."
        gen_report(content)
        sys.exit()
    time.sleep(5)

def search_component():
    el_span_button = driver.find_elements(By.CLASS_NAME, 'dijitButton')
        #print(el_span_button)
    for s in el_span_button:
        inner_span_text = s.find_element(By.CLASS_NAME, 'dijitButtonText')
            #print(inner_span_text.get_attribute('id') + '\t' + inner_span_text.get_attribute('innerHTML'))
        if inner_span_text.get_attribute('innerHTML') == 'Компонента поиска':
            s.click()

def docs_search(shk):
    driver.find_element(By.ID, 'dijit__TreeNode_1_label').click()
    el_input_search = wait.until(EC.visibility_of_element_located((By.ID, 'dijit_form_ValidationTextBox_0')))
    el_input_search.send_keys(shk)
    time.sleep(1)
    try:
        wait.until(EC.visibility_of_element_located((By.XPATH, "//span[contains(text(), 'Поиск')]"))).click()
    except:
        print(f"Поиск документов на 5 в КИС ЭХД недоступен.")
        sys.exit()

def kis_exit():
    driver.find_element(By.CLASS_NAME, 'userImage').click()
    time.sleep(1)
    driver.find_element(By.ID, "dijit_MenuItem_1_text").click()
    time.sleep(1)

cred = get_log_pas(url)
print('Логин={0}, пароль={1}, куратор={2}, пароль куратора={3}'.format(cred['tnlog'].partition('@')[0], cred['tnpas'], cred['curlog'], cred['curpas']))

options = Options()
options.binary_location = "C:\\Program Files\\Mozilla Firefox\\firefox.exe"
cService = webdriver.FirefoxService('geckodriver.exe')
driver = webdriver.Firefox(options=options, service=cService)
driver.implicitly_wait(180)
wait =WebDriverWait(driver, 180)

# Открыть страницу логина
t0 = time.time()
#print('T0={0}'.format(t0))
driver.get(url)
driver.maximize_window()
try:
    wait.until(EC.element_to_be_clickable((By.XPATH, "//span[contains(text(), 'Войти')]")))
    t1 = time.time()
    time_spended = t1 - t0
    content['login_page_time'] = round(time_spended, 2)
    print(f"Login page time: {time_spended:.2f} seconds")
except:
    print(f"Страница логина в КИС ЭХД недоступна.")
    content['login_page_time'] = "Страница логина в КИС ЭХД недоступна."
    gen_report(content)
    sys.exit()

# t1 = time.time()
# time_spended = t1 - t0
# print(f"Login page time: {time_spended:.2f} seconds")

# Логин в КИС
el_login = driver.find_element(By.CSS_SELECTOR, 'input[name="loginName"]')
el_pass = driver.find_element(By.CSS_SELECTOR, 'input[type="password"]')
el_domain_selector = driver.find_element(By.CSS_SELECTOR, 'input[value="▼ "]')
el_login_button = driver.find_element(By.XPATH, "//span[contains(text(), 'Войти')]")
el_login.send_keys(cred['tnlog'].partition('@')[0])
el_pass.send_keys(cred['tnpas'])
el_domain_selector.click()
time.sleep(1)
el_domain = driver.find_element(By.XPATH, "//td[contains(text(), '@tn.tngrp.ru')]") # ЭХД
time.sleep(1)
el_domain.click()
time.sleep(1)
t0 = time.time()
el_login_button.click()
try:
    wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(text(), 'Прочие действия')]")))
    t1 = time.time()
    time_spended = t1 - t0
    content['main_page_time'] = round(time_spended, 2)
    print(f"Main page time: {time_spended:.2f} seconds")
except:
    print(f"Главная страница КИС ЭХД недоступна.")
    content['main_page_time'] = "Главная страница КИС ЭХД недоступна."
    gen_report(content)
    sys.exit()
time.sleep(5)

# Поиск доков
driver.find_element(By.ID, 'dijit__TreeNode_1_label').click()
el_input_search = wait.until(EC.visibility_of_element_located((By.ID, 'dijit_form_ValidationTextBox_0')))
el_input_search.send_keys('5')
time.sleep(1)
try:
    wait.until(EC.visibility_of_element_located((By.ID, "dijit_form_Button_39"))).click()
except:
    print(f"Поиск документов на 5 в КИС ЭХД недоступен.")
    sys.exit()

# Просмотр карточки
time.sleep(5)
try:
    el_rows_checkbox = wait.until(EC.visibility_of_any_elements_located((By.CSS_SELECTOR, "span[role=checkbox]")))
    us_state = False
    num_try = 1
    check_us_connection = 'Неуспешно'
    while not us_state and num_try < 11:
        us_state = card_view(el_rows_checkbox, num_try)
        num_try += 1
    if us_state:
        check_us_connection = 'Успешно'
    content['doc_card'] = check_doc_card
    content['us_connection'] = check_us_connection
    print(f"Открытие карточки документа: {check_doc_card}")
    print(f"Проверка связи с УС: {check_us_connection}")
except:
    content['doc_card'] = 'Неуспешно'
    content['us_connection'] = 'Неуспешно'
    print(f"Карточка документов в КИС ЭХД недоступна.")
    gen_report(content)
    sys.exit()

# Выход из КИС
driver.find_element(By.CLASS_NAME, 'userImage').click()
time.sleep(1)
driver.find_element(By.ID, "dijit_MenuItem_1_text").click()
time.sleep(1)

# Проверка БП простого
# shutil.copyfile(test_files_path + '1770999995999.pdf', export_path + '1770999995999.pdf')
# shutil.copyfile(test_files_path + '1770999995999.xml', export_path + '1770999995999.xml')
# time.sleep(180)
# login_action(cur_name, cur_pass)
# docs_search('1770999995999')
# doc_delete()
# kis_exit()

gen_report(content)
