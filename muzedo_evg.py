from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import time
import jinja2
import datetime
import sys

content = {}
content['datetimenow'] = datetime.datetime.now().strftime('%d.%m.%Y %H:%M')
content['title'] = 'Не определено'

#url = 'https://muzedo-tnl.tn.tngrp.ru/UZDO/index.html' # TNL UZDO
#url = 'https://myzedo-tnt.tn.tngrp.ru/UZDO/index.html' # TNT UZDO
#url = 'https://muzedo-prm.tn.tngrp.ru/UZDO/index.html' # TPP UZDO
url = 'https://muzedo-tsd.tn.tngrp.ru/UZDO/index.html' #TSD UZDO
#url = 'https://muzedo-pvg.dc-prod.tn.corp/UZDO/index.html' # TPV UZDO
#url = 'https://muzedo-sib.tn.tngrp.ru/UZDO/index.html' #TSIB
#url = 'https://muzedo-npk.tn.tngrp.ru/UZDO/index.html' # TNPK
#url = 'https://muzedo-tur.tn.tngrp.ru/UZDO/index.html' #TUR

####Cred
tnlogin = 'prigozhineb-a@tn.tngrp.ru'
tnpas = '<eltnJctym2024('
dclog = 'PushkarevPA@dc-mgmt.tn.corp'
dcpas = 'pvJ^9kePqFpX[]^3!QPo'

def gen_report(content):
    # Jinja2
    loader = jinja2.FileSystemLoader('.')
    env = jinja2.Environment(loader=loader, trim_blocks=True)

    tpl = env.get_template('template-muzedo.txt')
    result = tpl.render(content)
    with open('otchet.html', 'wb') as fp:
        fp.write(result.encode('utf-8'))
    # Открыть страницу отчета
    driver.get('file:///Z:\УОПС\ОЭСЭД\FileNet\AL_Пушкарев\Scripts\otchet.html')

def get_log_pas(url):
    logpas = []
    #if 'tngrp' in url or 'dc-prod' in url:
    if 'qqqdc-prod' in url:
        logpas.append(dclog)
        logpas.append(dcpas)
    else:
        logpas.append(tnlogin)
        logpas.append(tnpas)    

    return logpas

def left_tree_nav():
    for i in range(1, 8):
        main_xpath = '/html/body/div[1]/div[3]/div/div/div[1]/div/div[3]/div/div[2]/div[' + str(i) +']/div[1]/span[1]'
        main_sign_xpath = '/html/body/div[1]/div[3]/div/div/div[1]/div/div[3]/div/div[2]/div[' + str(i) +']/div[1]/span[2]'
        el_main = driver.find_element(By.XPATH, main_xpath)
        el_main_sign = driver.find_element(By.XPATH, main_sign_xpath)
        if el_main_sign.get_attribute('textContent') == '-':
            el_main.click()
            time.sleep(1)

def get_journal_state(key):
    content[key] = []
    #print(f"Журнал {key}")
    xpath_journal = '/html/body/div[1]/div[3]/div/div/div[1]/div/div[3]/div/div[2]/div[7]/div[2]/div[2]/div[2]/div['+ str(journal_type[key]) + ']/div[1]/span[3]'
    el_journal_ehd = driver.find_element(By.XPATH, xpath_journal)
    el_journal_ehd.click()
    el_rows = wait.until(EC.visibility_of_any_elements_located((By.CLASS_NAME, 'gridxBody')))
    page_journal = driver.page_source
    soup_journal_ehd = BeautifulSoup(page_journal, 'lxml')
    el_rows = soup_journal_ehd.find_all('div', {'class': 'gridxRow'})

    row_string = 'Неуспешно'
    for i,row in enumerate(el_rows):
        if i < 5:
            el_w_title = row.find_all('div', {'title':True})
            row_string = ""
            for el in el_w_title:
                if len(el) > 0 and el.get_text() == 'Успешно':
                    row_string = 'Успешно'
                    break
        if row_string == 'Успешно':
            break
            #print(row_string)
    content[key].append(row_string)
    #print(content[key])

journal_type = {
    'ЭХД': 1,
    'НСИ': 2,
    'КСДД': 3,
    'СЭД': 4,
    'УС': 5,
    'УЦ': 6,
}

cred = get_log_pas(url)
items_journal = []
print('Логин={0} и пароль={1}'.format(cred[0].partition('@')[0], cred[1]))

cService = webdriver.FirefoxService('geckodriver.exe')
driver = webdriver.Firefox(service=cService)
driver.implicitly_wait(180)
wait =WebDriverWait(driver, 180)
t0 = time.time()
driver.get(url)
driver.maximize_window()
try:
    wait.until(EC.element_to_be_clickable((By.XPATH, "//span[contains(text(), 'Войти')]")))
    t1 = time.time()
    time_spended = t1 - t0
    content['login_page_time'] = round(time_spended, 2)
    print(f"Login page time: {time_spended:.2f} seconds")
except:
    print(f"Страница логина в КИС МЮЗ ЭДО недоступна.")
    content['login_page_time'] = "Страница логина в КИС МЮЗ ЭДО недоступна."
    gen_report(content)
    sys.exit()

el_login = driver.find_element(By.CSS_SELECTOR, 'input[name="loginName"]')
el_pass = driver.find_element(By.CSS_SELECTOR, 'input[type="password"]')
el_domain_selector = driver.find_element(By.CSS_SELECTOR, 'input[value="▼ "]')
el_login_button = driver.find_element(By.XPATH, "//span[contains(text(), 'Войти')]")

el_login.send_keys(cred[0].partition('@')[0])
el_pass.send_keys(cred[1])
el_domain_selector.click()

#el_domain = driver.find_element(By.XPATH, "//td[contains(text(), '@tn.tngrp.ru')]") # ЭХД
el_domain = driver.find_element(By.XPATH, "//div[contains(text(), '@tn.tngrp.ru')]") # мюзда
el_domain.click()
time.sleep(1)
t0 = time.time()
el_login_button.click()
try:
    wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(text(), 'Операции над ЮЗЭД')]")))
    t1 = time.time()
    time_spended = t1 - t0
    content['main_page_time'] = round(time_spended, 2)
    print(f"Main page time: {time_spended:.2f} seconds")
except:
    print(f"Главная страница КИС МЮЗ ЭДО недоступна.")
    content['main_page_time'] = "Главная страница КИС МЮЗ ЭДО недоступна."
    gen_report(content)
    sys.exit()
time.sleep(5)

content['title'] = driver.find_element(By.XPATH, '//*[@id="container_tablist_gp.views.HierarchyModule_0"]').get_attribute('textContent')

left_tree_nav()

el_journal = driver.find_element(By.XPATH, '/html/body/div[1]/div[3]/div/div/div[1]/div/div[3]/div/div[2]/div[7]/div[1]/span[1]')
el_journal.click()
time.sleep(1)
el_journal_integration = driver.find_element(By.XPATH, '/html/body/div[1]/div[3]/div/div/div[1]/div/div[3]/div/div[2]/div[7]/div[2]/div[2]/div[1]/span[1]')
el_journal_integration.click()

for key in journal_type:
    get_journal_state(key)
    time.sleep(1)

# Открываем карточку для проверки связи с УЦ
el_uc_row_checkbox = driver.find_element(By.XPATH, '/html/body/div[1]/div[3]/div/div/div[3]/div/div[2]/div/div[3]/div[4]/div[1]/table/tbody/tr/td/span')
#print(el_uc_row_checkbox)
el_uc_row_checkbox.click()
el_doc_open_btn = driver.find_element(By.XPATH, '//*[@id="dijit_form_Button_2789"]')
el_doc_open_btn.click()

# Проверяем подписи
EC.visibility_of_element_located((By.XPATH, '//iframe[@data-dojo-attach-point="viewer"]'))
time.sleep(15)
el_podpisi = driver.find_element(By.XPATH, "//span[contains(text(), 'Электронная подпись')]")
print(el_podpisi)
el_podpisi.click()
time.sleep(2)

content['ep_check_time'] = 'Неуспешно'
el_ep = driver.find_element(By.XPATH, "//span[contains(text(), 'Проверить ЭП')]")
el_ep_kontragent = driver.find_element(By.XPATH, "//span[contains(text(), 'Проверить ЭП контрагента')]")
if el_ep.value_of_css_property('cursor') == 'pointer':
    t0 = time.time()
    input_value = driver.find_element(By.XPATH, '//*[@id="dijit_form_ValidationTextBox_8"]').get_attribute('value')
    print(f"Старое значение {input_value}")
    el_ep.click()
    while driver.find_element(By.XPATH, '//*[@id="dijit_form_ValidationTextBox_8"]').get_attribute('value') == input_value:
        time.sleep(1/10)
    input_value = driver.find_element(By.XPATH, '//*[@id="dijit_form_ValidationTextBox_8"]').get_attribute('value')
    print(f"Значение: {input_value}")
    t1 = time.time()
    time_spended = t1 - t0
    print(f"Длительность проверки ЭП: {time_spended:.2f} seconds")
    content['ep_check_time'] = round(time_spended, 2)
elif el_ep_kontragent.value_of_css_property('cursor') == 'pointer':
    t0 = time.time()
    input_value = driver.find_element(By.XPATH, '//*[@id="dijit_form_ValidationTextBox_21"]').get_attribute('value')
    print(f"Старое значение {input_value}")
    el_ep_kontragent.click()
    while driver.find_element(By.XPATH, '//*[@id="dijit_form_ValidationTextBox_21"]').get_attribute('value') == input_value:
        time.sleep(1/10)
    input_value = driver.find_element(By.XPATH, '//*[@id="dijit_form_ValidationTextBox_21"]').get_attribute('value')
    print(f"Значение: {input_value}")
    t1 = time.time()
    time_spended = t1 - t0
    print(f"Длительность проверки ЭП контрагента: {time_spended:.2f} seconds")
    content['ep_check_time'] = round(time_spended, 2)
else:
    print('Неуспешно')
    content['ep_check_time'] = 'Неуспешно'

# Выход из КИС
driver.find_element(By.CLASS_NAME, 'userImage').click()
time.sleep(1)
driver.find_element(By.ID, "dijit_MenuItem_1_text").click()
time.sleep(1)

gen_report(content)